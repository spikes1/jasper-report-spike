package net.agmsolutions.jasperreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class JasperReportSpikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasperReportSpikeApplication.class, args);
	}

}
