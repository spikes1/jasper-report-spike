package net.agmsolutions.jasperreport.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.agmsolutions.jasperreport.model.BeanDTO;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@RestController
@RequestMapping("/api/report")
public class ReportController {

	@GetMapping("/")
	public ResponseEntity<byte[]> createReport() throws Exception {
		
		// TODO this is only an Example! Refactor the code please...
		
		List<BeanDTO> beanDTO = new ArrayList<BeanDTO>();

		BeanDTO bean1 = new BeanDTO();
		bean1.setCampoNumero(1);
		bean1.setCampoStringa("Stefy");
		beanDTO.add(bean1);

		BeanDTO bean2 = new BeanDTO();
		bean2.setCampoNumero(2);
		bean2.setCampoStringa("Patrick");
		beanDTO.add(bean2);

		JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(beanDTO);

		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("ReportDTODataSource", itemsJRBean);

		ClassPathResource classPathResource = new ClassPathResource("ReportDTO.jasper");
		InputStream inputStream = classPathResource.getInputStream();
		// il file JRXML compilato diventa .jasper e deve essere messo nelle resources
		// della app
		JasperPrint jasperPrint = JasperFillManager.fillReport(inputStream, parameters, new JREmptyDataSource());

		File pdfFile = File.createTempFile("report", ".pdf");
		OutputStream outputStream = new FileOutputStream(pdfFile);
		JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);

		System.out.println("File Generated");

		byte[] body = null;
		try {
			body = Files.readAllBytes(pdfFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ResponseEntity.ok().contentLength(pdfFile.length())
				.contentType(MediaType.parseMediaType("application/pdf")).body(body);
	}

}
