package net.agmsolutions.jasperreport.model;

import lombok.Data;

@Data
public class BeanDTO {

	String campoStringa;

	Integer campoNumero;

}
